CC = emcc
SRCS = $(wildcard src/*.c)
CFLAGS = -O1 -Wall -std=c99 -D_DEFAULT_SOURCE -Wno-missing-braces -Os -s USE_GLFW=3 -s ASSERTIONS=1 -s WASM=1 -s EMTERPRETIFY=1 -s  EMTERPRETIFY_ASYNC=1 
INC = -I. -I./include 
LIBB = -L./libs/  ./libs/libraylib.bc

all:
	$(CC) -o index.html $(SRCS) $(CFLAGS) --shell-file shell.html $(INC) $(LIBB) -DPLATFORM_WEB
clear:
	rm index.html index.js index.js.orig.js index.wasm
run:
	emrun index.html
